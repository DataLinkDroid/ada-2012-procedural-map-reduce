with Interfaces;
with Ada.Text_IO; use Ada.Text_IO;
with Ada.Real_Time;
with Ada.Numerics;

with System.Multiprocessors; use System.Multiprocessors;

with System.Multiprocessors.Dispatching_Domains;
use System.Multiprocessors.Dispatching_Domains;

with Params;


procedure Map_Reduce_Parallel is

   Num_CPUs  : CPU := Number_Of_CPUs;
   First_CPU : CPU := Get_First_CPU (System_Dispatching_Domain);
   Next_CPU  : CPU := First_CPU;
   Workers   : Positive := Positive (Number_Of_CPUs);

   subtype Float64 is Interfaces.IEEE_Float_64;
   subtype Longint is Long_Long_Integer;
   subtype Values_Index is Longint range 1 .. Params.Data_Points;
   use type Float64;
   use type Ada.Real_Time.Time;
   use type Ada.Real_Time.Time_Span;

   type Values_Array_Type is array (Values_Index) of Float64;
   type Values_Array_Access is access Values_Array_Type;
   type LoHi is (Low, High);

   subtype Worker_Range is Positive range  1 .. Workers;

   Idx_Ranges   : array (Worker_Range, Low .. High) of Values_Index;
   Values_Array : Values_Array_Access := new Values_Array_Type;

   task type Map_Reduce is
      entry Run (Start_Idx, Finish_Idx : in Values_Index);
      entry Get_Result (Result : out Float64);
   end Map_Reduce;

   task body Map_Reduce is
      My_Sum              : Float64 := 0.0;
      My_Start, My_Finish : Values_Index;
   begin
      loop
         select
            accept Run (Start_Idx, Finish_Idx : in Values_Index) do
               My_Sum := 0.0;
               My_Start := Start_Idx;
               My_Finish := Finish_Idx;
            end Run;
            for Idx in My_Start .. My_Finish loop
               declare
                  Val : Float64 renames Values_Array (Idx);
               begin
                  My_Sum := My_Sum + Val * Val;
               end;
            end loop;
         or
            accept Get_Result (Result : out Float64) do
               Result := My_Sum;
            end Get_Result;
         or
            terminate;
         end select;
      end loop;
   end Map_Reduce;

   Tasks : array (Positive range 1 .. Workers) of Map_Reduce;
