with Interfaces;
with Ada.Text_IO; use Ada.Text_IO;
with Ada.Real_Time;
with Ada.Numerics;

with Params;

procedure Map_Reduce_Main is

   subtype Float64 is Interfaces.IEEE_Float_64;
   type Values_Index is range 1 .. Params.Data_Points;
   use type Float64;
   use type Ada.Real_Time.Time;
   use type Ada.Real_Time.Time_Span;

   type Values_Array_Type is array (Values_Index) of Float64;
   type Values_Array_Access is access Values_Array_Type;

   Values_Array  : Values_Array_Access := new Values_Array_Type;

   procedure Calculation_Run
     (Sum_Of_Squares       : out Float64;
      Elapsed_Milliseconds : out Duration)
   is
      use Ada.Real_Time;
      Sum         : Float64 := 0.0;
      Start_Time  : Time := Clock;
      Finish_Time : Time;
   begin

      for Val of Values_Array.all loop
         Sum := Sum + Val * Val;
      end loop;

      Finish_Time := Clock;
      Sum_Of_Squares := Sum;
      Elapsed_Milliseconds := To_Duration (Finish_Time - Start_Time) * 1_000;

   end Calculation_Run;

begin

   -- Setup

   for I in Values_Index loop
      Values_Array (I) := Ada.Numerics.Pi;
   end loop;

   -- Do the work.

   declare
      Num_Runs      : constant := Params.Calculation_Runs;
      Totals        : array (1 .. Num_Runs) of Float64 := (others => 0.0);
      Elapsed_Times : array (1 .. Num_Runs) of Duration := (others => Duration (0));
      Mean_Time     : Duration;

      function Sum_Durations return Duration is
      begin
         return Sum : Duration := Duration (0) do
            for Elapsed of Elapsed_Times loop
               Sum := Sum + Elapsed;
            end loop;
         end return;
      end Sum_Durations;

   begin

      for Run in 1 .. Num_Runs loop
         Calculation_Run (Totals (Run), Elapsed_Times (Run));
      end loop;

      Mean_Time := Sum_Durations / Num_Runs;

      New_Line;
      Put_Line ("SINGLE-THREADED V2");
      Put_Line ("Data points    : " & Params.Data_Points'Image);
      Put_Line ("Sum of squares : " & Totals (1)'Image);
      Put_Line ("Duration (ms)  : " & Mean_Time'Image);

   end;

end Map_Reduce_Main;
