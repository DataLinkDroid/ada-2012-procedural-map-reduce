with Interfaces;
with Ada.Text_IO; use Ada.Text_IO;
with Ada.Real_Time;
with Ada.Numerics;

with System.Multiprocessors; use System.Multiprocessors;

with System.Multiprocessors.Dispatching_Domains;
use System.Multiprocessors.Dispatching_Domains;

with Params;


procedure Map_Reduce_Parallel is

   Num_CPUs  : CPU := Number_Of_CPUs;
   First_CPU : CPU := Get_First_CPU (System_Dispatching_Domain);
   Next_CPU  : CPU := First_CPU;
   Workers   : Positive := Positive (Number_Of_CPUs);

   subtype Float64 is Interfaces.IEEE_Float_64;
   subtype Longint is Long_Long_Integer;
   subtype Values_Index is Longint range 1 .. Params.Data_Points;
   use type Float64;
   use type Ada.Real_Time.Time;
   use type Ada.Real_Time.Time_Span;

   type Values_Array_Type is array (Values_Index) of Float64;
   type Values_Array_Access is access Values_Array_Type;
   type LoHi is (Low, High);

   subtype Worker_Range is Positive range  1 .. Workers;

   Idx_Ranges   : array (Worker_Range, Low .. High) of Values_Index;
   Values_Array : Values_Array_Access := new Values_Array_Type;

   task type Map_Reduce is
      entry Run (Start_Idx, Finish_Idx : in Values_Index);
      entry Get_Result (Result : out Float64);
   end Map_Reduce;

   task body Map_Reduce is
      My_Sum              : Float64 := 0.0;
      My_Start, My_Finish : Values_Index;
   begin
      loop
         select
            accept Run (Start_Idx, Finish_Idx : in Values_Index) do
               My_Sum := 0.0;
               My_Start := Start_Idx;
               My_Finish := Finish_Idx;
            end Run;
            for Idx in My_Start .. My_Finish loop
               declare
                  Val : Float64 renames Values_Array (Idx);
               begin
                  My_Sum := My_Sum + Val * Val;
               end;
            end loop;
         or
            accept Get_Result (Result : out Float64) do
               Result := My_Sum;
            end Get_Result;
         or
            terminate;
         end select;
      end loop;
   end Map_Reduce;

   Tasks : array (Positive range 1 .. Workers) of Map_Reduce;

   procedure Set_Worker_Index_Ranges
   is
      My_Step : Values_Index :=
                  (Values_Index'Last - Values_Index'First + 1) / Longint (Workers);
   begin

      for Worker in 1 .. Workers loop
         Idx_Ranges (Worker, Low) := Values_Index'First + Longint (Worker - 1) * My_Step;
         Idx_Ranges (Worker, High) := Longint (Worker) * My_Step;
      end loop;

      Idx_Ranges (Workers, High) := Values_Index'Last;

   end Set_Worker_Index_Ranges;

   procedure Calculation_Run
     (Sum_Of_Squares       : out Float64;
      Elapsed_Milliseconds : out Duration)
   is
      use Ada.Real_Time;
      Sums        : array (Worker_Range) of Float64 := (others => 0.0);
      Total       : Float64 := 0.0;
      Start_Time  : Time := Clock;
      Finish_Time : Time;
   begin

      for Worker in Worker_Range loop
         Tasks (Worker).Run
           (Start_Idx  => Idx_Ranges (Worker, Low),
            Finish_Idx => Idx_Ranges (Worker, High));
      end loop;

      for Worker in Worker_Range loop
         Tasks (Worker).Get_Result (Sums (Worker));
      end loop;

      for Worker in Worker_Range loop
         Total := Total + Sums (Worker);
      end loop;

      Finish_Time := Clock;
      Sum_Of_Squares := Total;
      Elapsed_Milliseconds := To_Duration (Finish_Time - Start_Time) * 1_000;

   end Calculation_Run;

begin

   -- Setup

   Set_CPU (First_CPU);
   Set_Worker_Index_Ranges;

   for I in Values_Index loop
      Values_Array (I) := Ada.Numerics.Pi;
   end loop;

   for Worker_Idx in Worker_Range loop
      Set_CPU (Next_CPU, Tasks (Worker_Idx)'Identity);
      Next_CPU := Next_CPU + 1;
   end loop;

   -- Do the work.

   declare

      Num_Runs      : constant := Params.Calculation_Runs;
      Totals        : array (1 .. Num_Runs) of Float64 := (others => 0.0);
      Elapsed_Times : array (1 .. Num_Runs) of Duration := (others => Duration (0));
      Mean_Time     : Duration;

      function Sum_Durations return Duration is
      begin
         return Sum : Duration := Duration (0) do
            for Elapsed of Elapsed_Times loop
               Sum := Sum + Elapsed;
            end loop;
         end return;
      end Sum_Durations;

   begin

      for Run in 1 .. Num_Runs loop
         Calculation_Run (Totals (Run), Elapsed_Times (Run));
      end loop;

      Mean_Time := Sum_Durations / Num_Runs;

      New_Line;
      Put_Line ("MULTI-THREADED V2");
      Put_Line ("Data points    : " & Params.Data_Points'Image);
      Put_Line ("Worker tasks   : " & Workers'Image);
      Put_Line ("Sum of squares : " & Totals (1)'Image);
      Put_Line ("Duration (ms)  : " & Mean_Time'Image);

   end;

end Map_Reduce_Parallel;
