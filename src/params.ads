package Params is
   Data_Points      : constant := 1024_000_000;
   Calculation_Runs : constant := 50;
end Params;
